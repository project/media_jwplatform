<?php

/**
 * @file
 * Admin Related Functionality.
 */

/**
 * Admin Configuration FORM.
 */
function media_jwplatform_admin_form($form, $form_state) {

  $form['jwplatform'] = array(
    '#type' => 'fieldset',
    '#title' => t('JW Platform API Configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['jwplatform']['media_jwplatform_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('JW Platform Key'),
    '#size' => 25,
    '#default_value' => variable_get('media_jwplatform_api_key', ''),
  );

  $form['jwplatform']['media_jwplatform_api_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('JW Platform Secret'),
    '#size' => 25,
    '#default_value' => variable_get('media_jwplatform_api_secret', ''),
  );

  // Pull all players list.
  if (!empty(variable_get('media_jwplatform_api_key', ''))) {

    $form['jwplatform_players'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('JW Platform Players'),
      '#collapsible' => TRUE,
    );

    $form['jwplatform_players']['media_jwplatform_default_player'] = array(
      '#title'         => t('Default Video Player'),
      '#type'          => 'select',
      '#options'       => MediaJwplatformSearch::getPlayerList(),
      '#default_value' => variable_get('media_jwplatform_default_player'),
      '#empty_option'  => t('None Selected'),
    );
  }

  return system_settings_form($form);
}
