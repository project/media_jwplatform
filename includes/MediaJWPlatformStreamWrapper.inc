<?php

/**
 * @file
 * Extends the MediaReadOnlyStreamWrapper class to handle YouTube videos.
 */

/**
 * Create an instance like this.
 *
 * $youtube = new MediaYouTubeStreamWrapper('youtube://v/[video-code]');
 */
class MediaJWPlatformStreamWrapper extends MediaReadOnlyStreamWrapper {

  /**
   * Returns mimetype.
   */
  public static function getMimeType($uri, $mapping = NULL) {
    return 'video/jwplatform';
  }

  /**
   * Get a video id.
   */
  public function getVideoId() {
    if ($url = parse_url($this->uri)) {
      if ($url['scheme'] == 'jwplatform') {
        return $url['host'];
      }
    }

    return NULL;
  }

  /**
   * Returns interpolateURL.
   */
  public function interpolateUrl() {
    return $this->getLocalThumbnailPath();
  }

  /**
   * Provides Local Thumbnail Path.
   */
  public function getLocalThumbnailPath() {

    $params = $this->get_parameters();
    $styles = '';

    if (is_array($params) and isset($params['styles'])) {
      $styles = $params['styles'];
      $video_key = $params['jwplatform'];
    }
    else {
      $url = parse_url($this->uri);
      $video_key = $url['host'];
    }

    $possible_exts = array('jpg', 'jpeg', 'png', 'gif');
    foreach ($possible_exts as $ext) {
      $local_path = 'public://media-jwplatform/' . $video_key . ".$ext";
      if (file_exists($local_path)) {
        if (!empty($styles)) {
          return file_create_url("public://styles/$styles/public/media-jwplatform/$video_key" . ".$ext");
        }
        return $local_path;
      }
    }

    return '';
  }

}
