<?php

/**
 * @file
 * Definition of MediaBrowserUpload.
 */

/**
 * Media browser plugin for showing the upload form.
 *
 * @deprecated
 */
class MediaJWPlatformBrowser extends MediaBrowserPlugin {

  /**
   * Implements MediaBrowserPluginInterface::access().
   */
  public function access($account = NULL) {
    return file_entity_access('create', NULL, $account);
  }

  /**
   * Implements MediaBrowserPlugin::view().
   */
  public function view() {
    $filter_form = drupal_get_form('media_jwplatform_browser_filter_form');
    $filter_form = drupal_render($filter_form);
    $jwpath = drupal_get_path('module', 'media_jwplatform');

    return array(
      '#title' => t('JW Platform Videos'),
      '#settings' => array(
    // 'viewMode' => 'thumbnails',.
        'getMediaUrl' => url('jwplatform/media/list'),
        'multiselect' => FALSE,
    // 'types' => isset($params['types']) ? $params['types'] : array(),
      ),
      '#attached' => array(
        'library' => array(),
        'js' => array(
          $jwpath . '/js/jwplatform_media.js',
          $jwpath . '/js/jwplatform_media.video.js',
        ),
      ),
    // If the #form and #markup parameters are not empty the media module will
    // not render the fake submit button.
      '#form' => array(FALSE),
      '#markup' => '<div id="container"><div id="scrollbox">' . $filter_form . '</div></div>',
    );
  }

}
