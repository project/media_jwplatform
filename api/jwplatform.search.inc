<?php

/**
 * @file
 * A wrapper for jwplatform search.
 */

/**
 * Class to communicate to jwplatform request.
 */
class MediaJwplatformSearch {

  private static $version = '1.4';
  private static $url = 'http://api.jwplatform.com/v1';
  private static $key;
  private static $secret;

  /**
   * Initial configuration.
   */
  public static function init() {

    self::$key    = variable_get('media_jwplatform_api_key');
    self::$secret = variable_get('media_jwplatform_api_secret');

  }

  /**
   * Provide player list.
   */
  public static function getPlayerList() {

    if (empty(self::$key)) {
      self::init();
    }

    $players_list = &drupal_static(__CLASS__ . '::' . __FUNCTION__);
    if (isset($players_list)) {
      return $players_list;
    }

    $cid   = 'jwplatform:players:full_list';
    $cache = media_jwplatform_cache_get($cid);

    if ($cache) {
      $players_list = $cache;
    }
    else {
      $result = self::call("/players/list", array());
      if ($result['status'] == 'ok' and count($result['players'])) {
        foreach ($result['players'] as $player) {
          $players_list[$player['key']] = $player['name'];
        }
      }

      media_jwplatform_cache_set($cid, $players_list);
    }

    return $players_list;
  }

  /**
   * Urlencode function.
   */
  private static function urlEncode($input) {

    if (is_array($input)) {
      return array_map(array('_urlencode'), $input);
    }
    else {
      if (is_scalar($input)) {
        return str_replace('+', ' ', str_replace('%7E', '~', rawurlencode($input)));
      }
      else {
        return '';
      }
    }
  }

  /**
   * Sign API call arguments.
   */
  private static function sign($args) {

    ksort($args);
    $sbs = "";
    foreach ($args as $key => $value) {
      if ($sbs != "") {
        $sbs .= "&";
      }
      // Construct Signature Base String.
      $sbs .= self::urlEncode($key) . "=" . self::urlEncode($value);
    }

    // Add shared secret to the Signature Base.
    $signature = sha1($sbs . self::$secret);

    return $signature;
  }

  /**
   * Add required api_* arguments.
   */
  private static function args($args) {

    $args['api_nonce']     = str_pad(mt_rand(0, 99999999), 8, STR_PAD_LEFT);
    $args['api_timestamp'] = time();

    $args['api_key'] = self::$key;

    if (!array_key_exists('api_format', $args)) {
      // Use the serialised PHP format,
      // otherwise use format specified in the call() args.
      $args['api_format'] = 'php';
    }

    // Add API kit version.
    $args['api_kit'] = 'php-' . self::$version;

    // Sign the array of arguments.
    $args['api_signature'] = self::sign($args);

    return $args;
  }

  /**
   * Make an API call.
   */
  public static function call($call, $args = array()) {

    if (empty(self::$key)) {
      self::init();
    }

    $arguments = self::args($args);
    $query     = http_build_query($arguments, '', '&');
    $url       = self::$url . $call . '?' . $query;

    $result = drupal_http_request($url);

    if ($result->status_message === 'OK') {

      $unserialized_response = @unserialize($result->data);
      return $unserialized_response ? $unserialized_response : $result->data;

    }

    return array();
  }

  /**
   * Pull thumbnail.
   */
  public static function pullVideoThumbnail($video_key) {

    $thumb_url = "http://content.jwplatform.com/thumbs/$video_key.jpg";

    $parts      = explode('.', $thumb_url);
    $ext        = $parts[count($parts) - 1];
    $local_path = 'public://media-jwplatform/' . $video_key . '.' . strtolower($ext);

    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      @copy($thumb_url, $local_path);
    }

    return $local_path;

  }

}
