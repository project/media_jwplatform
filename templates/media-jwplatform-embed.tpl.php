<?php

/**
 * @file
 * Template File to show.
 */

if(empty($player_id)) {
    $player_id = variable_get('media_jwplatform_default_player', '');
}
?>
<script src="//content.jwplatform.com/players/<?php print $video_id; ?>-<?php print $player_id; ?>.js"></script>
