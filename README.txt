Media JWPlatform

Installation

1. Download the module and install it
2. Go to : Configuration > Media > JW Platform
  2.1 Provide JW Platform API & Secret key then press save
  2.2 If credentials were correct another option to choose Default
      Video Player will show up. This will show all configured
      players form JW Platform
3. Go to : Structure > File Types > Video > Manage File Display
  3.1 Under **Default** check **JW PlatformVideo**
  3.2 Under **Teaser** check **JW Platform Preview Image**
  3.3 Under **Preview** check **JW Platform Preview Image**
