/**
 * @file
 * Provides functions for the jwplatform module media browser integration.
 */

(function ($) {
  'use strict';
  Drupal.jwplatform = Drupal.jwplatform || {};
  Drupal.jwplatform.library = Drupal.jwplatform.library || {};
  Drupal.jwplatform.library.video = Drupal.jwplatform.library.video || {};
  Drupal.jwplatform.library.video.loaded = Drupal.jwplatform.library.video.loaded || false;

  Drupal.behaviors.jwplatformVideoBrowser = {
    attach: function (context, settings) {
      Drupal.jwplatform.library.video = new Drupal.jwplatform.library(Drupal.settings.media.browser.jwplatform);
      $('#media-browser-tabset', context).bind(
        'tabscreate tabsshow tabsactivate', function (event, ui) {
          var panel = event.type === 'tabsactivate' ? ui.newPanel : ui.panel;

          if (typeof panel != 'undefined' && $(panel).attr('id') === 'media-tab-jwplatform') {
            // Prevent reloading of media list on tabselect if already loaded in media list.
            if (!Drupal.jwplatform.library.video.loaded) {
              var params = {};
              for (var p in Drupal.settings.media.browser.jwplatform) {
                if (Drupal.settings.media.browser.jwplatform.hasOwnProperty(p)) {
                  params[p] = Drupal.settings.media.browser.jwplatform[p];
                }
              }

              Drupal.jwplatform.library.video.start($(panel), params);
              $('#scrollbox').bind('scroll', Drupal.jwplatform.library.video, Drupal.jwplatform.library.video.scrollUpdater);
              Drupal.jwplatform.library.video.loaded = true;
            }
          }
        }
      );

      $('#edit-filter').not('.bc-processed').addClass('bc-processed').click(
        function (ev) {
          ev.preventDefault();

          // Acquire filter form values.
          var searchVal = $(Drupal.jwplatform.library.video.renderElement).find('.search-radio:checked').val();
          var keywordsVal = $(Drupal.jwplatform.library.video.renderElement).find('#edit-keywords').val();
          // Set library object parameters (used for ajax loading new media into list)
          Drupal.jwplatform.library.video.params.filter = Drupal.jwplatform.library.video.params.filter || {};
          Drupal.jwplatform.library.video.params.filter = {search: searchVal, keywords: keywordsVal};

          // Remove the media list.
          Drupal.jwplatform.library.video.cursor = 0;
          Drupal.jwplatform.library.video.mediaFiles = [];
          $(Drupal.jwplatform.library.video.renderElement).find('#media-browser-library-list li').remove();
          $('#scrollbox').unbind('scroll').bind('scroll', Drupal.jwplatform.library.video, Drupal.jwplatform.library.video.scrollUpdater);

          // Set a flag so we don't make multiple concurrent AJAX calls.
          Drupal.jwplatform.library.video.loading = true;
          // Reload the media list.
          Drupal.jwplatform.library.video.loadMedia();
        }
      );

      $('#edit-reset').not('.bc-processed').addClass('bc-processed').click(
        function (ev) {
          ev.preventDefault();

          // Reset filter form values.
          delete Drupal.jwplatform.library.video.params.filter;
          $(Drupal.jwplatform.library.video.renderElement).find('.search-radio[value=name]').attr('checked', true);
          $(Drupal.jwplatform.library.video.renderElement).find('#edit-keywords').val('');
          $('#scrollbox').unbind('scroll').bind('scroll', Drupal.jwplatform.library.video, Drupal.jwplatform.library.video.scrollUpdater);

          // Remove the media list.
          Drupal.jwplatform.library.video.cursor = 0;
          Drupal.jwplatform.library.video.mediaFiles = [];
          $(Drupal.jwplatform.library.video.renderElement).find('#media-browser-library-list li').remove();
          // Set a flag so we don't make multiple concurrent AJAX calls.
          Drupal.jwplatform.library.video.loading = true;
          // Reload the media list.
          Drupal.jwplatform.library.video.loadMedia();
        }
      );

      $(document).delegate(
        '#media-browser-library-list a', 'mousedown', function () {
          var uri = $(this).attr('data-uri');
          $("input[name='submitted-video']").val(uri);
          var file = {uri: uri};
          var files = [];
          files.push(file);
          Drupal.media.browser.selectMedia(files);
        }
      );
    }
  };

})(jQuery);
