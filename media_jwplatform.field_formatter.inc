<?php

/**
 * @file
 * Jwplatform based Field Formatter.
 */

/**
 * File Formatter Player Settings.
 */
function media_jwplatform_file_formatter_video_settings($form, $form_state, $settings) {
  $form = array();
  $form['player'] = array(
    '#title' => t('Player'),
    '#type' => 'radios',
    '#options' => MediaJwplatformSearch::getPlayerList(),
    '#default_value' => isset($settings['player']) ? $settings['player'] : '',
  );
  return $form;
}

/**
 * File Formatter view.
 */
function media_jwplatform_file_formatter_video_view($file, $display, $langcode) {

  $scheme = file_uri_scheme($file->uri);
  if ($scheme == 'jwplatform' && empty($file->override['wysiwyg'])) {

    $element = array(
      '#theme'    => 'media_jwplatform_embed',
      '#type'     => $scheme,
      '#video_id' => parse_url($file->uri, PHP_URL_HOST),
      '#player_id'   => $display['settings']['player'],
      '#attached' => array(),
    );

    return $element;
  }

  return NULL;

}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_jwplatform_file_formatter_image_settings($form, &$form_state, $settings) {
  $element = array();

  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#options' => image_style_options(FALSE),
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
  );

  return $element;
}

/**
 * Formatter Image View.
 */
function media_jwplatform_file_formatter_image_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);
  if ($scheme == 'jwplatform') {
    $image_style = $display['settings']['image_style'];

    // In case of media popup we do not have local thumbnail.
    if (isset($file->curl_thumbnail)) {
      return array(
        '#theme'      => 'image',
        '#path'       => $file->curl_thumbnail,
        '#attributes' => array(
          'height' => 150,
        ),
      );
    }
    else {
      $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
      return array(
        '#theme'      => 'image_style',
        '#style_name' => empty($image_style) ? 'media_thumbnail' : $image_style,
        '#path'       => $wrapper->getLocalThumbnailPath(),
        '#alt'        => isset($file->override['attributes']['alt']) ? $file->override['attributes']['alt'] : $file->filename,
      );
    }
  }

  return NULL;

}
